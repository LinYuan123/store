package edu.lingnan.store.utils;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Component
public class UploadUtil {

    public String uploadPhoto(MultipartFile picture, String filePath) throws IOException {
        if(!picture.isEmpty()){
            String imgName = UUID.randomUUID().toString().replace("-","");
            //获取原始文件名并截取图片后缀名
            String fileName = picture.getOriginalFilename();
            String imaLastName = fileName.substring(fileName.lastIndexOf("."));
            //创建本地文件流，存放图片路径
            File oldfile = new File(filePath);
            File file = new File(oldfile.getAbsolutePath()+ File.separator+imgName+imaLastName);//.getAbsolutePath()返回绝对路径
            //写入磁盘
            picture.transferTo(file);
//            String picName = imgName+imaLastName;
            return imgName+imaLastName;
        }
        return null;
    }

}
