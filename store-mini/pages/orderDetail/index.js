import { request } from "../../request/index.js";
import dateUtils from '../../utils/dateUtils.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    totalNum:0,
    totalPrice:0,
    hideModel:false,
    order:{},
    orderDetail:[],
    pickway:true
  },

  onShow() {

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getOrderInfo(options.orderId);
  },
  //获取订单信息
  async getOrderInfo(orderId){
    console.log("orderId:"+orderId)
    const res = await request({ url: "order/info", data: {id:orderId}, method: 'GET' });
    console.log(res);
    let order = res.data.order;
    let pickway = true;
    if(order.pickupWay==="deliver"){
      order.pickupWay="配送";
      pickway = true;
    }else{
      order.pickupWay="自提";
      pickway = false;
    }
    order.createTime = dateUtils(order.createTime,"Y-M-D h:m:s");
    let orderDetail = res.data.details;
    this.setData({
      order,orderDetail,pickway
    })
  }
})