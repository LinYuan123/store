package edu.lingnan.store.controller;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.common.ResultBean;
import edu.lingnan.store.entity.UserRole;
import edu.lingnan.store.service.UserRoleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * (UserRole)表控制层
 *
 * @author yinrui
 * @since 2021-04-13 23:54:24
 */
@RestController
@RequestMapping("userRole")
public class UserRoleController {
    /**
     * 服务对象
     */
    @Resource
    private UserRoleService userRoleService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public UserRole selectOne(Integer id) {
        return this.userRoleService.queryById(id);
    }

    /**
     * 根据条件查询
     * @param page
     * @param limit
     * @param bean
     * @return
     */
    @GetMapping("list")
    public ResultBean getUserRoleData(@RequestParam("page") Integer page,
                                   @RequestParam("limit") Integer limit,
                                   String bean){
        ResultBean resultBean = new ResultBean<>();
        UserRole userRole = JSONUtil.toBean(bean,UserRole.class);
        IPage<UserRole> iPage = userRoleService.queryAllByLimit(page,limit,userRole);
        Map<String, Object> map = new HashMap<>();
        map.put("total",iPage.getTotal());
        map.put("list",iPage.getRecords());
        resultBean.setData(map);
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

    @GetMapping("/info")
    public ResultBean getUserRoleInfo(@RequestParam("id") Integer id) {
        ResultBean resultBean = new ResultBean<>();
        if(id == null || id < 0){
            resultBean.setSuccess(false);
            return resultBean;
        }
        UserRole userRole = userRoleService.queryById(id);
        if(userRole!=null){
            resultBean.setData(userRole);
        }
        return resultBean;
    }

    @PostMapping("save")
    public ResultBean saveUserRole(@RequestBody UserRole bean){
        ResultBean resultBean = new ResultBean();
        int count = 0;
        if(bean.getId()!=null && bean.getId()>0){
            count = userRoleService.update(bean);
        }else{
            count = userRoleService.insert(bean);
        }
        resultBean.setSuccess(count>0);
        if(count<=0){
            resultBean.setMsg("保存失败！");
            return resultBean;
        }
        resultBean.setMsg("保存成功！");
        return resultBean;
    }

    @PostMapping("delete")
    public ResultBean delete(Integer id){
        ResultBean resultBean = new ResultBean<>();
        resultBean.setSuccess(userRoleService.deleteById(id));
        if(!resultBean.isSuccess()){
            resultBean.setMsg("删除失败！");
            return resultBean;
        }
        resultBean.setMsg("删除成功！");
        return resultBean;
    }

}