package edu.lingnan.store.common;

/**
 *通用返回对象
 */
public class ResultBean<T> {

    private String msg;
    private Integer code = 200;
    private boolean isSuccess = true;
    private T data;

    public ResultBean() {
    }

    public ResultBean(String msg, boolean isSuccess, T data) {
        this.msg = msg;
        this.isSuccess = isSuccess;
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
