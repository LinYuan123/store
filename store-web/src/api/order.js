//订单相关请求
import axios from '../plugins/http.js'

/**
 * 获取订单列表
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_orderList(params) {
    return axios({
        url: '/order/list',
        method: 'get',
        params: params,
    })
}

export function api_orderReminderList(params) {
    return axios({
        url: '/order/orderReminder',
        method: 'get',
        params: params,
    })
}

/**
 * 修改订单状态
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_updateOrderStatus(params) {
    return axios({
        url: '/order/updateStatus',
        method: 'post',
        data: params,
    })
}

/**
 * 获取订单信息
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_orderInfo(params) {
    return axios({
        url: '/order/info',
        method: 'get',
        params: params,
    })
}

/**
 * 保存订单信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_saveOrder(params) {
    return axios({
        url: '/order/save',
        method: 'post',
        data: params,
    })
}

/**
 * 删除订单信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_deleteOrder(params) {
    return axios({
        url: '/order/delete',
        method: 'post',
        data: params,
    })
}

