package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * (GoodsType)实体类
 *
 * @author yinrui
 * @since 2021-01-28 16:57:06
 */
@Getter
@Setter
@ToString
public class GoodsType implements Serializable {
    private static final long serialVersionUID = -69380364904647550L;
    
    private Integer id;
    /**
    * 父级分类ID
    */
    private Integer pid;
    
    private String name;

    private String pname;
    
    private String status;

}