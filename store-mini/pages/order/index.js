import { request } from "../../request/index.js";
import dateUtils from '../../utils/dateUtils.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: [
      { id: 0, value: "全部", status:null, isActive: true },
      { id: 1, value: "待收货", status:"send", isActive: false },
      { id: 2, value: "已备货", status:"stockup", isActive: false }
    ],
    orders: [],
    orderDetail:[],
    orderStatus:{},
    type:null,
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const userInfo = wx.getStorageSync('userinfo')
    if(userInfo===null || userInfo===""){
      wx.navigateTo({
        url: '/pages/login/index'
      })
      return
    }
    //获取小程序的页面栈-数组 长度最大是10
    var curPages = getCurrentPages();
    //数组中最大索引的值即为当前页面
    let currPage = curPages[curPages.length - 1];
    //获取URL上的type参数
    const { type } = currPage.options;
    //根据type值激活选中订单查询页面的标题
    this.changeTitleByIndex(type - 1);
    this.getOrders(type);
  },
   //标题的点击事件 从子组件传递过来
  handleTabsItemChange(e) {
    //1.获取被点击的标题索引
    const { index } = e.detail;
    this.changeTitleByIndex(index);
    //2.重新发送请求获取列表数据
    this.getOrders(index + 1);
    this.setData({
      type:index+1
    })
  },
  //根据标题索引来激活选中标题数据
  changeTitleByIndex(index) {
    //2.修改源数据 产生激活选中效果
    let { tabs } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    //3.赋值到data中
    this.setData({
        tabs
    })
  },
  //获取订单列表async 
  async getOrders(type) {
    console.log(type)
    let status = "";
    switch(type){
      case "1":
        status = "";
        break;
      case 2:
        status = "send";
        break;
      case 3:
        status = "stockup";
        break;
      case "2":
        status = "send";
        break;
      case "3":
        status = "stockup";
        break;
    }
    const userInfo = wx.getStorageSync('userinfo')
    let param = {
      userId:userInfo.id,
      status:status
    }
    const res = await request({ url: "order/userOrders", data: param, method: 'GET' });
    let orders = res.data.orders;
    let orderDetail = res.data.orderDetails;
    let orderStatus = this.data.orderStatus;
    orders.forEach(v=>{
      v.createTime = dateUtils(v.createTime,"Y-M-D h:m:s");
      switch(v.status){
        //cancel、paid、stockup、send、finish
        case "CANCEL":
          v.status = "已取消";
          orderStatus[v.id] = false;
          break;
        case "PAID":
          v.status = "已支付";
          orderStatus[v.id] = false;
          break;
        case "STOCKUP":
          v.status = "已备货";
          orderStatus[v.id] = true;
          break;
        case "SEND":
          v.status = "待收货";
          orderStatus[v.id] = true;
          break;
        case "FINISH":
          v.status = "已完成";
          orderStatus[v.id] = false;
          break;
      }
    })
    this.setData({
      orders,orderDetail,orderStatus
    })
  },
  async btnFinish(e){
    //确认收货
    const status = "FINISH";
    const orderId = e.currentTarget.dataset.orderid;
    let ids = []
    ids.push(orderId)
    const param = {
      idsJson: JSON.stringify(ids)
    }
    const res = await request({ url: "order/finishOrder", data: param, method: 'POST' });
    wx.showToast({
      title: res.msg,
      icon: 'none',
      mask: true
    });
    wx.redirectTo({
      url: '/pages/order/index?type=1'
    })
  },
  toOrderDetail(e){
    const orderId = e.currentTarget.dataset.orderid;
    //带参数跳转
    wx.navigateTo({
      url: '/pages/orderDetail/index?orderId='+orderId,
    })
  }

})