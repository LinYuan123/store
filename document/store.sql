/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50532
 Source Host           : localhost:3306
 Source Schema         : store

 Target Server Type    : MySQL
 Target Server Version : 50532
 File Encoding         : 65001

 Date: 28/02/2021 20:00:34
*/package edu.lingnan.tmall.aspect;

import edu.lingnan.tmall.common.api.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author yinrui
 */
@Aspect
@Component
@Slf4j
public class CommonResultControllerAspect {
    /*
        1.定义切入点
        2.实现环绕
     */
    @Pointcut("execution(public * edu.lingnan.tmall.*.controller..*.*(..)) ")
    public void pointcut(){
    }

    @Around("pointcut()")
    public Object aroundAuthAround(ProceedingJoinPoint pjp) throws Throwable {
        log.info("aroundAuthAround");
        Method method = getMethod(pjp);
        System.out.println(method);
        Object result = pjp.proceed();
        System.out.println(result);
        CommonResult commonResult = CommonResult.success(result);
        if(result instanceof Map){
            Map map = (Map) result;
            if(map.containsKey("msg")){
                String msg = String.valueOf(map.get("msg"));
                map.remove("msg");
                commonResult = CommonResult.success(result,msg);
            }
        }
        return commonResult;
    }

    private Method getMethod(ProceedingJoinPoint pjp){
        MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
        return methodSignature.getMethod();
    }

}


SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for discount
-- ----------------------------
DROP TABLE IF EXISTS `discount`;
CREATE TABLE `discount`  (
  `ID` int(10) NOT NULL,
  `TYPE` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `AMOUNT` double NULL DEFAULT NULL COMMENT '满减金额',
  `CREATE_TIME` datetime NULL DEFAULT NULL,
  `STATUS` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of discount
-- ----------------------------

-- ----------------------------
-- Table structure for evaluation
-- ----------------------------
DROP TABLE IF EXISTS `evaluation`;
CREATE TABLE `evaluation`  (
  `ID` int(10) NOT NULL,
  `USER_ID` int(10) NULL DEFAULT NULL,
  `GOODS_ID` int(10) NULL DEFAULT NULL,
  `GOODS_NAME` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `ORDER_ID` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `SCORE` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL COMMENT '1-10分',
  `DETAIL` varchar(255) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `CREATE_TIME` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `e1`(`USER_ID`) USING BTREE,
  INDEX `e2`(`GOODS_ID`) USING BTREE,
  INDEX `e3`(`ORDER_ID`) USING BTREE,
  CONSTRAINT `e1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `e2` FOREIGN KEY (`GOODS_ID`) REFERENCES `goods` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `e3` FOREIGN KEY (`ORDER_ID`) REFERENCES `order` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of evaluation
-- ----------------------------

-- ----------------------------
-- Table structure for evaluation_picture
-- ----------------------------
DROP TABLE IF EXISTS `evaluation_picture`;
CREATE TABLE `evaluation_picture`  (
  `ID` int(10) NOT NULL,
  `EVALUATION_ID` int(10) NULL DEFAULT NULL,
  `LOCATION` varchar(255) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `ep1`(`EVALUATION_ID`) USING BTREE,
  CONSTRAINT `ep1` FOREIGN KEY (`EVALUATION_ID`) REFERENCES `evaluation` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of evaluation_picture
-- ----------------------------

-- ----------------------------
-- Table structure for evaluation_reply
-- ----------------------------
DROP TABLE IF EXISTS `evaluation_reply`;
CREATE TABLE `evaluation_reply`  (
  `ID` int(10) NOT NULL,
  `EVALUATION_ID` int(10) NULL DEFAULT NULL,
  `TEXT` varchar(500) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `USER_ID` int(10) NULL DEFAULT NULL,
  `LAST_REPLY_ID` int(10) NULL DEFAULT NULL,
  `CREATE_TIME` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `er1`(`EVALUATION_ID`) USING BTREE,
  INDEX `er2`(`USER_ID`) USING BTREE,
  CONSTRAINT `er1` FOREIGN KEY (`EVALUATION_ID`) REFERENCES `evaluation` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `er2` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of evaluation_reply
-- ----------------------------

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback`  (
  `ID` int(10) NOT NULL,
  `USER_ID` int(10) NULL DEFAULT NULL,
  `CREATE_TIME` datetime NULL DEFAULT NULL,
  `TEXT` varchar(500) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `CONTACT` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `fb1`(`USER_ID`) USING BTREE,
  CONSTRAINT `fb1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of feedback
-- ----------------------------

-- ----------------------------
-- Table structure for focus
-- ----------------------------
DROP TABLE IF EXISTS `focus`;
CREATE TABLE `focus`  (
  `ID` int(10) NOT NULL,
  `USER_ID` int(10) NULL DEFAULT NULL,
  `GOODS_ID` int(10) NULL DEFAULT NULL,
  `GOODS_NAME` varchar(255) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `CREATE_TIME` date NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `fo1`(`USER_ID`) USING BTREE,
  INDEX `FO2`(`GOODS_ID`) USING BTREE,
  CONSTRAINT `fo1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FO2` FOREIGN KEY (`GOODS_ID`) REFERENCES `goods` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of focus
-- ----------------------------

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `ID` int(10) NOT NULL,
  `NAME` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `TYPE_ID` int(10) NULL DEFAULT NULL,
  `PRICE` decimal(20, 2) NULL DEFAULT NULL,
  `STOCK` int(20) NULL DEFAULT NULL,
  `TOTAL_SALES` int(10) NULL DEFAULT NULL,
  `MONTHLY_SALES` int(10) NULL DEFAULT NULL,
  `DISCOUNT` decimal(20, 2) NULL DEFAULT NULL COMMENT '优惠',
  `STATUS` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `FOCUS_NUMBER` int(10) NULL DEFAULT NULL COMMENT '收藏数量',
  `FILE` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL COMMENT '图片地址',
  `CREATE_TIME` date NULL DEFAULT NULL COMMENT '上架时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `g1`(`TYPE_ID`) USING BTREE,
  CONSTRAINT `g1` FOREIGN KEY (`TYPE_ID`) REFERENCES `goods_type` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of goods
-- ----------------------------

-- ----------------------------
-- Table structure for goods_type
-- ----------------------------
DROP TABLE IF EXISTS `goods_type`;
CREATE TABLE `goods_type`  (
  `ID` int(10) NOT NULL,
  `PID` int(10) NULL DEFAULT NULL COMMENT '父级分类ID',
  `NAME` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `STATUS` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of goods_type
-- ----------------------------

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `ID` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NOT NULL,
  `USER_ID` int(10) NULL DEFAULT NULL,
  `ADDRESS` varchar(255) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `PICKUP_WAY` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL COMMENT '提货方式',
  `DELIVERY_TIME` datetime NULL DEFAULT NULL COMMENT '配送/取货时间',
  `PAY_WAY` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL COMMENT '付款方式',
  `CREATE_TIME` datetime NULL DEFAULT NULL,
  `STATUS` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `DELIVER_MAN` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `DELIVER_MAN_CONTACT` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `ORIGINAL_PRICE` double(20, 2) NULL DEFAULT NULL COMMENT '原始价格',
  `DISCOUNT` double(20, 2) NULL DEFAULT NULL COMMENT '优惠',
  `PRESENT_PRICE` double(20, 2) NULL DEFAULT NULL COMMENT '折后价格',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for order_detail
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail`  (
  `ID` int(10) NOT NULL,
  `ORDER_ID` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `GOODS_ID` int(10) NULL DEFAULT NULL,
  `GOODS_NAME` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `AMOUNT` int(10) NULL DEFAULT NULL,
  `GOODS_PRICE` double(20, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `od1`(`ORDER_ID`) USING BTREE,
  INDEX `od2`(`GOODS_ID`) USING BTREE,
  CONSTRAINT `od1` FOREIGN KEY (`ORDER_ID`) REFERENCES `order` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `od2` FOREIGN KEY (`GOODS_ID`) REFERENCES `goods` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of order_detail
-- ----------------------------

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `ID` int(10) NOT NULL,
  `NAME` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `STATUS` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `CREATE_TIME` date NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permission
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `ID` int(10) NOT NULL,
  `NAME` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `STATUS` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `CREATE_TIME` date NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission`  (
  `ID` int(10) NOT NULL,
  `ROLE_ID` int(10) NULL DEFAULT NULL,
  `PERMISSION_ID` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `rp1`(`ROLE_ID`) USING BTREE,
  INDEX `rp2`(`PERMISSION_ID`) USING BTREE,
  CONSTRAINT `rp1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `rp2` FOREIGN KEY (`PERMISSION_ID`) REFERENCES `permission` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role_permission
-- ----------------------------

-- ----------------------------
-- Table structure for shop_cart
-- ----------------------------
DROP TABLE IF EXISTS `shop_cart`;
CREATE TABLE `shop_cart`  (
  `ID` int(10) NOT NULL,
  `USER_ID` int(10) NULL DEFAULT NULL,
  `AMOUNT` int(10) NULL DEFAULT NULL,
  `GOODS_ID` int(10) NULL DEFAULT NULL,
  `GOODS_NAME` varchar(255) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `UPDATE_TIME` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `sc1`(`USER_ID`) USING BTREE,
  INDEX `sc2`(`GOODS_ID`) USING BTREE,
  CONSTRAINT `sc1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sc2` FOREIGN KEY (`GOODS_ID`) REFERENCES `goods` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of shop_cart
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `LOGIN_NAME` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `NAME` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `PASSWORD` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `CREATE_TIME` date NULL DEFAULT NULL,
  `LAST_LOGIN_TIME` datetime NULL DEFAULT NULL,
  `SEX` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `INTEGRAL` int(10) NULL DEFAULT NULL COMMENT '积分',
  `CONTACT` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `EMAIL` varchar(255) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL COMMENT '邮件',
  `ADDRESS` varchar(255) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL,
  `HEAD_PORTRAIT` varchar(255) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL COMMENT '头像图片地址',
  `STATUS` varchar(50) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci NULL DEFAULT NULL COMMENT '账号状态',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', '管理员', 'admin', '2021-02-21', NULL, '男', NULL, '13643256323', '3456324423@qq.com', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `ID` int(10) NOT NULL,
  `USER_ID` int(10) NULL DEFAULT NULL,
  `ROLE_ID` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `ur1`(`USER_ID`) USING BTREE,
  INDEX `ur2`(`ROLE_ID`) USING BTREE,
  CONSTRAINT `ur1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ur2` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = gb2312 COLLATE = gb2312_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_role
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
