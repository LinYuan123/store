package edu.lingnan.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.entity.GoodsType;
import edu.lingnan.store.entity.GoodsTypeWithChildren;

import java.util.List;

/**
 * (GoodsType)表服务接口
 *
 * @author makejava
 * @since 2021-01-28 16:57:06
 */
public interface GoodsTypeService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GoodsType queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    IPage<GoodsType> queryAllByLimit(int offset, int limit, GoodsType bean);

    /**
     * 新增数据
     *
     * @param goodsType 实例对象
     * @return 实例对象
     */
    int insert(GoodsType goodsType);

    /**
     * 修改数据
     *
     * @param goodsType 实例对象
     * @return 实例对象
     */
    int update(GoodsType goodsType);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    List<GoodsTypeWithChildren> listWithChildren();

    List<GoodsType> parentList();

}