package edu.lingnan.store.enums;

import lombok.Getter;

@Getter
public enum OrderEnum {

    CANCEL("已取消"),
    PAID("已支付"),
    STOCKUP("已备货"),
    SEND("已送出"),
    FINISH("已完成");

    private String value;

    OrderEnum(String value){
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
