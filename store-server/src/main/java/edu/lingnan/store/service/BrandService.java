package edu.lingnan.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.entity.Brand;

import java.util.List;

/**
 * (Brand)表服务接口
 *
 * @author yinrui
 * @since 2021-04-03 14:15:52
 */
public interface BrandService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Brand queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    IPage<Brand> queryAllByLimit(int offset, int limit,Brand brand);

    /**
     *
     * @return
     */
    List<Brand> queryAllBrand();

    /**
     * 新增数据
     *
     * @param brand 实例对象
     * @return 实例对象
     */
    int insert(Brand brand);

    /**
     * 修改数据
     *
     * @param brand 实例对象
     * @return 实例对象
     */
    int update(Brand brand);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}