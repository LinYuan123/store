//品牌相关请求
import axios from '../plugins/http.js'

/**
 * 返回所有品牌列表
 */
export function api_allBrandList() {
  return axios.get('brand/brandList')
}
/**
 * 返回所有分类列表
 */
export function api_brandList(params) {
  return axios({
    url: '/brand/list',
    method: 'get',
    params: params,
  })
}

/**
 * 获取品牌信息
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_brandInfo(params) {
  return axios({
    url: '/brand/info',
    method: 'get',
    params: params,
  })
}

/**
 * 保存品牌信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_saveBrand(params) {
  return axios({
    url: '/brand/save',
    method: 'post',
    data: params,
  })
}

/**
 * 删除品牌信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_deleteBrand(params) {
  return axios({
    url: '/brand/delete',
    method: 'post',
    data: params,
  })
}
