//import Vue from 'vue'
import router from './index.js'

//白名单
const whileList = ['/login','/forget']
//将导航守卫挂载到router
router.beforeEach((to, from, next) => {
  //
  if (whileList.indexOf(to.path) !== -1) {
    next()
  } else {
    const token = window.sessionStorage.getItem('token')
    if (token) {
      next()
    } else {
      next('/')
    }
    next()
  }
})
