//管理员登录相关请求
import axios from '../plugins/http.js'

/**
 * 登入
 * @param username
 * @param password
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_login(loginAccount, password) {
  return axios.post('/user/login', {
    loginAccount,
    password, //ES6语法
  })
}

/**
 * 登出
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_logout() {
  return axios.get('/user/loginOut')
}

/**
 * 刷新token
 * @param map
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_refreshToken(token) {
  return axios.post('/user/refreshToken', {
    token, //ES6语法
  })
}

/**
 * 获取用户信息
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_info(userId) {
  return axios.post('/user/info', {
    userId,
  })
}

/**
 * 发送验证码请求
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_sendCode(param) {
  return axios({
    url: '/upload/sendCode',
    method: 'post',
    data: param,
  })
}

export function api_resetPassword(params) {
  return axios({
    url: '/user/resetPassword',
    method: 'post',
    data: params,
  })
}