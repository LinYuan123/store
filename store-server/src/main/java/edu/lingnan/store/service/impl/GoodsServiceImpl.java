package edu.lingnan.store.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.lingnan.store.dao.GoodsDao;
import edu.lingnan.store.entity.Goods;
import edu.lingnan.store.entity.OrderDetail;
import edu.lingnan.store.service.GoodsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Goods)表服务实现类
 *
 * @author yinrui
 * @since 2021-01-28 16:57:06
 */
@Service("goodsService")
public class GoodsServiceImpl implements GoodsService {
    @Resource
    private GoodsDao goodsDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Goods queryById(Integer id) {
        return this.goodsDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @param bean 实例对象
     * @return 对象列表
     */
    @Override
    public IPage<Goods> queryAllByLimit(int offset, int limit, Goods bean) {
        Page<Goods> page = new Page<>(offset, limit);
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        wrapper.like("name",bean.getName())
                .eq("type_id",bean.getTypeId())
                .gt("create_time",bean.getBeginTime())
                .lt("create_time",bean.getEndTime());
        page.setRecords(goodsDao.queryAll(page,bean));
        page.setTotal(page.getRecords().size());
        return page;
    }

    @Override
    public List<Goods> getAllProduct(){
        return this.goodsDao.getAllProduct();
    }

    @Override
    public List<Goods> getAllNew(){
        return this.goodsDao.getAllNew();
    }

    @Override
    public List<Goods> getStock(List<OrderDetail> details){
        return this.goodsDao.getStock(details);
    }

    @Override
    public List<Goods> getLowStocks() {
        return this.goodsDao.getLowStocks();
    }

    @Override
    public List<Goods> getSalesRank() {
        return this.goodsDao.getSalesRank();
    }

    @Override
    public int updateGoodsSale(Integer goodsId,Integer num) {
        return this.goodsDao.updateGoodsSale(goodsId, num);
    }

    @Override
    public int resetMonthlySales(){
        return this.goodsDao.resetMonthlySales();
    }

    /**
     * 新增数据
     *
     * @param goods 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(Goods goods) {
        return this.goodsDao.insert(goods);
    }

    /**
     * 修改数据
     *
     * @param goods 实例对象
     * @return 实例对象
     */
    @Override
    public int update(Goods goods) {
        return this.goodsDao.update(goods);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.goodsDao.deleteById(id) > 0;
    }

    /**
     * 批量修改状态
     *
     * @param ids 实例对象
     * @return 实例对象
     */
    @Override
    public int changeGoodsStatus(Integer[] ids, String type, Integer status) {
        return this.goodsDao.changeGoodsStatus(ids,type,status);
    }


}