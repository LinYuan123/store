package edu.lingnan.store.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.lingnan.store.dao.MenuDao;
import edu.lingnan.store.entity.Menu;
import edu.lingnan.store.service.MenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Menu)表服务实现类
 *
 * @author makejava
 * @since 2021-03-28 16:39:46
 */
@Service("menuService")
public class MenuServiceImpl implements MenuService {
    @Resource
    private MenuDao menuDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Menu queryById(Integer id) {
        return this.menuDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public IPage<Menu> queryAllByLimit(int offset, int limit, Menu bean) {
        Page<Menu> page = new Page<>(offset, limit);
        QueryWrapper<Menu> wrapper = new QueryWrapper<>();
        wrapper.like("name",bean.getName());
        page.setRecords(this.menuDao.queryAll(page,bean));
        page.setTotal(page.getRecords().size());
        return page;
    }

    @Override
    public List<Menu> parentList(){
        return this.menuDao.parentList();
    }

    /**
     * 新增数据
     *
     * @param menu 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(Menu menu) {
        return this.menuDao.insert(menu);
    }

    /**
     * 修改数据
     *
     * @param menu 实例对象
     * @return 实例对象
     */
    @Override
    public int update(Menu menu) {
        return this.menuDao.update(menu);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.menuDao.deleteById(id) > 0;
    }
}