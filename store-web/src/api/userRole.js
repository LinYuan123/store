//权限管理相关请求
import axios from '../plugins/http.js'

/**
 * 返回所有角色列表
 */
export function api_allRoleList() {
    return axios.get('role/roleList')
}
//getNoRoleUsers
export function api_NoRoleUsers() {
    return axios.get('user/noRoleUsers')
}

/**
 * 返回所有用户权限列表
 */
export function api_userRoleList(params) {
    return axios({
        url: '/userRole/list',
        method: 'get',
        params: params,
    })
}

/**
 * 获取用户权限信息
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_userRoleInfo(params) {
    return axios({
        url: '/userRole/info',
        method: 'get',
        params: params,
    })
}

/**
 * 保存用户权限信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_saveUserRole(params) {
    return axios({
        url: '/userRole/save',
        method: 'post',
        data: params,
    })
}

/**
 * 删除用户权限信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_deleteUserRole(params) {
    return axios({
        url: '/userRole/delete',
        method: 'post',
        data: params,
    })
}
