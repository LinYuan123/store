package edu.lingnan.store.controller;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.common.ResultBean;
import edu.lingnan.store.entity.Menu;
import edu.lingnan.store.service.MenuService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Menu)表控制层
 *
 * @author makejava
 * @since 2021-03-28 16:39:46
 */
@RestController
@RequestMapping("menu")
public class MenuController {
    /**
     * 服务对象
     */
    @Resource
    private MenuService menuService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Menu selectOne(Integer id) {
        return this.menuService.queryById(id);
    }

    /**
     * 根据条件查询菜单数据
     * @param page
     * @param limit
     * @param bean
     * @return
     */
    @GetMapping("list")
    public ResultBean getMenuData(@RequestParam("page") Integer page,
                                      @RequestParam("limit") Integer limit,
                                      String bean){
        System.out.println(bean);
        ResultBean resultBean = new ResultBean<>();
        Menu menu = JSONUtil.toBean(bean,Menu.class);
        IPage<Menu> iPage = this.menuService.queryAllByLimit(page,limit,menu);
        Map<String, Object> map = new HashMap<>();
        map.put("total",iPage.getTotal());
        map.put("list",iPage.getRecords());
        resultBean.setData(map);
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

    @GetMapping("parentList")
    public ResultBean parentTypeList(){
        ResultBean resultBean = new ResultBean();
        List<Menu> list = this.menuService.parentList();
        resultBean.setData(list);
        return resultBean;
    }

    @GetMapping("/info")
    public ResultBean getMenuInfo(@RequestParam("id") Integer id) {
        ResultBean<Menu> resultBean = new ResultBean<>();
        if(id == null || id < 0){
            resultBean.setSuccess(false);
            return resultBean;
        }
        Menu menu = this.menuService.queryById(id);
        if(menu!=null){
            resultBean.setData(menu);
        }
        return resultBean;
    }

    @PostMapping("save")
    public ResultBean saveMenu(@RequestBody Menu bean){
        ResultBean resultBean = new ResultBean();
        int count = 0;
        if(bean.getId()!=null && bean.getId()>0){
            count = this.menuService.update(bean);
        }else{
            bean.setCreateTime(new Date());
            count = this.menuService.insert(bean);
        }
        resultBean.setSuccess(count>0);
        if(count<=0){
            resultBean.setMsg("保存失败！");
            return resultBean;
        }
        resultBean.setMsg("保存成功！");
        return resultBean;
    }

    @PostMapping("delete")
    public ResultBean delete(Integer id){
        ResultBean resultBean = new ResultBean<>();
        resultBean.setSuccess(this.menuService.deleteById(id));
        if(!resultBean.isSuccess()){
            resultBean.setMsg("删除失败！");
            return resultBean;
        }
        resultBean.setMsg("删除成功！");
        return resultBean;
    }

}