package edu.lingnan.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.entity.Order;
import edu.lingnan.store.entity.OrderDetail;

import java.util.List;

/**
 * (Order)表服务接口
 *
 * @author makejava
 * @since 2021-01-28 16:57:06
 */
public interface OrderService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Order queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    IPage<Order> queryAllByLimit(int offset, int limit, Order order);

    List<Order> queryOrderByUserIdAndStatus(Integer userId, String status);

    List<Order> getOrdersByStatusAndPickupWay(String pickupWay, String status);

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    int insert(Order order);

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    int update(Order order);

    String save(Order order, List<OrderDetail> orderDetails);

    /**
     * 批量修改状态
     *
     * @param ids
     * @param status
     */
    boolean changeOrderStatus(String[] ids, String status);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    OrderDetail queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<OrderDetail> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param orderDetail 实例对象
     * @return 实例对象
     */
    OrderDetail insert(OrderDetail orderDetail);

    /**
     * 修改数据
     *
     * @param orderDetail 实例对象
     * @return 实例对象
     */
    OrderDetail update(OrderDetail orderDetail);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteByOrderId(String id);

}