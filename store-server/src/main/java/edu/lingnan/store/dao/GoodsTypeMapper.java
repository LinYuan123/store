package edu.lingnan.store.dao;

import edu.lingnan.store.entity.GoodsTypeWithChildren;

import java.util.List;

/**
 * @author yinrui
 */
public interface GoodsTypeMapper {

    List<GoodsTypeWithChildren> listWithChildren();

}
