package edu.lingnan.store.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.lingnan.store.dao.GoodsTypeDao;
import edu.lingnan.store.dao.GoodsTypeMapper;
import edu.lingnan.store.entity.Goods;
import edu.lingnan.store.entity.GoodsType;
import edu.lingnan.store.entity.GoodsTypeWithChildren;
import edu.lingnan.store.service.GoodsTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (GoodsType)表服务实现类
 *
 * @author makejava
 * @since 2021-01-28 16:57:06
 */
@Service("goodsTypeService")
public class GoodsTypeServiceImpl implements GoodsTypeService {
    @Resource
    private GoodsTypeDao goodsTypeDao;
    @Resource
    private GoodsTypeMapper goodsTypeMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public GoodsType queryById(Integer id) {
        return this.goodsTypeDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public IPage<GoodsType> queryAllByLimit(int offset, int limit, GoodsType bean) {
        Page<GoodsType> page = new Page<>(offset, limit);
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        wrapper.like("name",bean.getName());
        page.setRecords(goodsTypeDao.queryAll(page,bean));
        page.setTotal(page.getRecords().size());
        return page;
    }

    /**
     * 新增数据
     *
     * @param goodsType 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(GoodsType goodsType) {
        return this.goodsTypeDao.insert(goodsType);
    }

    /**
     * 修改数据
     *
     * @param goodsType 实例对象
     * @return 实例对象
     */
    @Override
    public int update(GoodsType goodsType) {
        return this.goodsTypeDao.update(goodsType);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.goodsTypeDao.deleteById(id) > 0;
    }

    @Override
    public List<GoodsTypeWithChildren> listWithChildren() {
        return this.goodsTypeMapper.listWithChildren();
    }

    @Override
    public List<GoodsType> parentList(){
        return this.goodsTypeDao.parentList();
    }


}