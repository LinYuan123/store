package edu.lingnan.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.entity.Goods;
import edu.lingnan.store.entity.OrderDetail;

import java.util.List;

/**
 * (Goods)表服务接口
 *
 * @author yinrui
 * @since 2021-01-28 16:57:06
 */
public interface GoodsService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Goods queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @param bean 实例对象
     * @return 对象列表
     */
    IPage<Goods> queryAllByLimit(int offset, int limit, Goods bean);

    List<Goods> getAllProduct();

    List<Goods> getAllNew();

    List<Goods> getStock(List<OrderDetail> details);

    List<Goods> getLowStocks();

    List<Goods> getSalesRank();

    int updateGoodsSale(Integer goodsId,Integer num);

    int resetMonthlySales();

    /**
     * 新增数据
     *
     * @param goods 实例对象
     * @return 实例对象
     */
    int insert(Goods goods);

    /**
     * 修改数据
     *
     * @param goods 实例对象
     * @return 实例对象
     */
    int update(Goods goods);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    int changeGoodsStatus(Integer[] ids, String type, Integer status);

}