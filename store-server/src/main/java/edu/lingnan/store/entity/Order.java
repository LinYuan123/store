package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * (Order)实体类
 *
 * @author makejava
 * @since 2021-01-28 16:57:06
 */
@Getter
@Setter
@ToString
public class Order extends QueryBean implements Serializable {
    private static final long serialVersionUID = 630181349558597095L;
    
    private String id;//UUID.randomUUID().toString();
    
    private Integer userId;
    
    private String address;
    /**
    * 提货方式
    */
    private String pickupWay;
    /**
    * 配送/取货时间---或改为订单完成时间
    */
    private Date deliveryTime;
    /**
    * 付款方式---默认在线支付
    */
    private String payWay;
    
    private Date createTime;
    
    private String status;
    
    private String deliverMan;
    
    private String deliverManContact;
    /**
    * 原始价格
    */
    private Double originalPrice;
    /**
    * 优惠
    */
    private Double discount;
    /**
    * 折后价格
    */
    private Double presentPrice;
    /**
     * 商品总数
     */
    private Integer totalNum;
    /**
     * 预留电话
     */
    private String contact;
    /**
     * 收货人
     */
    private String consignee;

}