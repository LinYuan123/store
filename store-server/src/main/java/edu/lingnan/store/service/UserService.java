package edu.lingnan.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.common.ResultBean;
import edu.lingnan.store.entity.User;

import java.util.List;

/**
 * (User)表服务接口
 *
 * @author makejava
 * @since 2021-01-28 16:57:07
 */
public interface UserService {

    ResultBean login(User user);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    IPage<User> queryAllByLimit(int offset, int limit, User bean);

    List<User> getNoRoleUsers();

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    int insert(User user);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    int update(User user);

    int changeUserStatus(Integer[] ids, String status);

    int updateUserIntegral(Integer integral, Integer userId);

    int getUserIntegral(Integer userId);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    User loadUserByLoginName(String loginName);

    List<User> checkUserExist(User user);

    List<User> checkUserRight(User user);

}