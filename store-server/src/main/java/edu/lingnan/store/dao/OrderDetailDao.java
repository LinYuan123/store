package edu.lingnan.store.dao;

import edu.lingnan.store.entity.OrderDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (OrderDetail)表数据库访问层
 *
 * @author makejava
 * @since 2021-01-28 16:57:07
 */
public interface OrderDetailDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    OrderDetail queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<OrderDetail> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param orderDetail 实例对象
     * @return 对象列表
     */
    List<OrderDetail> queryAll(OrderDetail orderDetail);

    List<OrderDetail> getDetailsByOrderId(String orderId);

    List<OrderDetail> getDetailsByUserIdAndStatus( @Param("userId")Integer userId, @Param("status")String status);

    /**
     * 新增数据
     *
     * @param orderDetail 实例对象
     * @return 影响行数
     */
    int insert(OrderDetail orderDetail);

    /**
     * 修改数据
     *
     * @param orderDetail 实例对象
     * @return 影响行数
     */
    int update(OrderDetail orderDetail);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteByOrderId(String id);

}