package edu.lingnan.store.service.impl;

import edu.lingnan.store.entity.ShopCart;
import edu.lingnan.store.dao.ShopCartDao;
import edu.lingnan.store.service.ShopCartService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (ShopCart)表服务实现类
 *
 * @author yinrui
 * @since 2021-04-10 14:04:29
 */
@Service("shopCartService")
public class ShopCartServiceImpl implements ShopCartService {
    @Resource
    private ShopCartDao shopCartDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ShopCart queryById(Integer id) {
        return this.shopCartDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<ShopCart> queryAllByLimit(int offset, int limit) {
        return this.shopCartDao.queryAllByLimit(offset, limit);
    }

    @Override
    public List<ShopCart> getCart(Integer userId){
        return this.shopCartDao.getCart(userId);
    }

    @Override
    public int setCart(List<ShopCart> carts){
        return this.shopCartDao.setCart(carts);
    }

    @Override
    public int deleteCartByUserId(Integer userId){
        return this.shopCartDao.deleteCartByUserId(userId);
    }

    @Override
    public int deleteCartByUserIdAndGoodsId(Integer userId,Integer goodsId){
        return this.shopCartDao.deleteCartByUserIdAndGoodsId(userId,goodsId);
    }

    /**
     * 新增数据
     *
     * @param shopCart 实例对象
     * @return 实例对象
     */
    @Override
    public ShopCart insert(ShopCart shopCart) {
        this.shopCartDao.insert(shopCart);
        return shopCart;
    }

    /**
     * 修改数据
     *
     * @param shopCart 实例对象
     * @return 实例对象
     */
    @Override
    public ShopCart update(ShopCart shopCart) {
        this.shopCartDao.update(shopCart);
        return this.queryById(shopCart.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.shopCartDao.deleteById(id) > 0;
    }
}