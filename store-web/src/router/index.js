import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login', // 转发路径
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/forget',
    name: 'Forget',
    // 懒加载，使用到的时候才会加载
    component: () => import('../views/Forget.vue'),
  },
  {
    path: '/home',
    name: 'Home',
    redirect: '/welcome',
    // 懒加载：使用到的时候才会加载
    component: () => import('../views/Home.vue'),
    children: [
      {
        path: '/about',
        name: 'About',
        // 懒加载：使用到的时候才会加载
        component: () => import('../views/About.vue'),
      },
      {
        path: '/welcome',
        name: 'Welcome',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/Welcome.vue'),
      },
      {
        path: '/product/productList',
        name: 'ProductList',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/product/ProductList.vue'),
      },
      {
        path: '/product/productEdit',
        name: 'ProductEdit',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/product/ProductEdit.vue'),
      },
      {
        path: '/product/productTypeList',
        name: 'ProductTypeList',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/productType/ProductTypeList.vue'),
      },
      {
        path: '/product/ProductTypeEdit',
        name: 'ProductTypeEdit',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/productType/ProductTypeEdit.vue'),
      },
      {
        path: '/product/brandList',
        name: 'BrandList',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/brand/BrandList.vue'),
      },
      {
        path: '/order/orderList',
        name: 'OrderList',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/order/OrderList.vue'),
      },
      {
        path: '/order/orderView',
        name: 'OrderView',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/order/OrderView.vue'),
      },
      {
        path: '/system/menuList',
        name: 'MenuList',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/menu/MenuList.vue'),
      },
      {
        path: '/system/menuEdit',
        name: 'MenuEdit',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/menu/MenuEdit.vue'),
      },
      {
        path: '/system/userList',
        name: 'UserList',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/user/UserList.vue'),
      },
      {
        path: '/system/userEdit',
        name: 'UserEdit',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/user/UserEdit.vue'),
      },
      {
        path: '/system/userRoleList',
        name: 'UserRoleList',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/userRole/UserRoleList.vue'),
      },
      {
        path: '/feedback/feedbackList',
        name: 'FeedbackList',
        // 懒加载，使用到的时候才会加载
        component: () => import('../views/feedback/FeedbackList.vue'),
      },
    ],
  },
]

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err)
}

const router = new VueRouter({
  routes,
})

export default router
