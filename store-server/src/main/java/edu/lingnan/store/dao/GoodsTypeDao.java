package edu.lingnan.store.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (GoodsType)表数据库访问层
 *
 * @author yinrui
 * @since 2021-01-28 16:57:06
 */
public interface GoodsTypeDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GoodsType queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<GoodsType> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit, @Param("goodsType") GoodsType goodsType);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param goodsType 实例对象
     * @return 对象列表
     */
    List<GoodsType> queryAll(IPage<GoodsType> page, @Param("goodsType") GoodsType goodsType);

    /**
     * 新增数据
     *
     * @param goodsType 实例对象
     * @return 影响行数
     */
    int insert(GoodsType goodsType);

    /**
     * 修改数据
     *
     * @param goodsType 实例对象
     * @return 影响行数
     */
    int update(GoodsType goodsType);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    List<GoodsType> parentList();

}