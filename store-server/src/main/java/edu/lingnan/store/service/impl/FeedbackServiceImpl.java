package edu.lingnan.store.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.lingnan.store.dao.FeedbackDao;
import edu.lingnan.store.entity.Feedback;
import edu.lingnan.store.service.FeedbackService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (Feedback)表服务实现类
 *
 * @author yinrui
 * @since 2021-04-15 00:08:05
 */
@Service("feedbackService")
public class FeedbackServiceImpl implements FeedbackService {
    @Resource
    private FeedbackDao feedbackDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Feedback queryById(Integer id) {
        return this.feedbackDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public IPage<Feedback> queryAllByLimit(int offset, int limit, Feedback bean) {
        Page<Feedback> page = new Page<>(offset, limit);
        page.setRecords(feedbackDao.queryAll(page,bean));
        page.setTotal(page.getRecords().size());
        return page;
    }

    /**
     * 新增数据
     *
     * @param feedback 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(Feedback feedback) {
        return this.feedbackDao.insert(feedback);
    }

    /**
     * 修改数据
     *
     * @param feedback 实例对象
     * @return 实例对象
     */
    @Override
    public int update(Feedback feedback) {
        return this.feedbackDao.update(feedback);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.feedbackDao.deleteById(id) > 0;
    }

    @Override
    public int changeFeedbackStatus(Integer[] ids, String status){
        return this.feedbackDao.changeFeedbackStatus(ids, status);
    }
}