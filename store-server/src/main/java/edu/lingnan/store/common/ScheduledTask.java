package edu.lingnan.store.common;

import edu.lingnan.store.service.GoodsService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 定时任务
 */
@Component
public class ScheduledTask {

    @Resource
    private GoodsService goodsService;

    /**
     * 每月1号0点执行 重置月销量
     */
    @Scheduled(cron="0 0 0 1 * ? ")
    public void resetMonthlySales(){
        System.out.println("==============重置月销量开始==============");
        goodsService.resetMonthlySales();
        System.out.println("==============重置月销量结束==============");
    }

}
