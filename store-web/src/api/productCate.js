//商品分类相关请求
import axios from '../plugins/http.js'

/**
 * 返回带有所有类别的子类别
 */
export function api_listWithChildren() {
  return axios.get('goodsType/listWithChildren')
}

/**
 * 返回所有分类列表
 */
export function api_productTypeList(params) {
  return axios({
    url: '/goodsType/list',
    method: 'get',
    params: params,
  })
}

/**
 * 返回所有父级分类
 */
export function api_parentTypeList() {
  return axios.get('goodsType/parentList')
}

/**
 * 获取商品分类信息
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_productTypeInfo(params) {
  return axios({
    url: '/goodsType/info',
    method: 'get',
    params: params,
  })
}

/**
 * 保存商品分类信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_saveProductType(params) {
  return axios({
    url: '/goodsType/save',
    method: 'post',
    data: params,
  })
}

/**
 * 删除商品分类信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_deleteProductType(params) {
  return axios({
    url: '/goodsType/delete',
    method: 'post',
    data: params,
  })
}
