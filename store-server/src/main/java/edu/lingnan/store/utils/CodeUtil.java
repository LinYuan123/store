package edu.lingnan.store.utils;

import java.util.Random;
import java.util.UUID;

public class CodeUtil {

    public static String getUUID32(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String randomCode() {
        StringBuilder str = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            str.append(random.nextInt(10));
        }
        return str.toString();
    }

}
