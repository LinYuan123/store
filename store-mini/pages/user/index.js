import { request } from "../../request/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo:{},
    integral:0,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //从缓存中获取token，没有则跳转登录页面
  },
  onShow(){
    let userinfo = wx.getStorageSync('userinfo');
    if(!userinfo){
      userinfo = {
        avatarUrl:'../../icons/user_default.png'
      }
    }
    this.setData({userinfo})
    this.getIntegral(userinfo.id)
  },
  async getIntegral(userId){
    const res = await request({ url: "user/getIntegral", data: {userId:userId}, method: 'GET' });
    let integral = res.data
    this.setData({
      integral
    })
  },
  wxAddress(){
    wx.chooseAddress({
      success: function (res) {
        //cityName: "广州市"
        // countyName: "海珠区"
        // detailInfo: "新港中路397号"
        // errMsg: "chooseAddress:ok"
        // nationalCode: "510000"
        // postalCode: "510000"
        // provinceName: "广东省"
        // telNumber: "020-81167888"
        // userName: "张三"
        var address = {
          "name": res.userName,
          "phone": res.telNumber,
          "province": res.provinceName,
          "city": res.cityName,
          "county": res.countyName,
          "detailInfo": res.detailInfo,
        };
        console.log(res);
        //  //获取到的地址存到data里的areaList中
        // that.setData({     
        //   areaList:that.data.areaList.push(address)
        // });
      },
      fail: (err) => {
        console.log(err)
      }
    })
  }
})