package edu.lingnan.store.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * (Permission)实体类
 *
 * @author makejava
 * @since 2021-01-28 16:57:07
 */
public class Permission implements Serializable {
    private static final long serialVersionUID = -78511336963788181L;
    
    private Integer id;
    
    private String name;
    
    private String status;
    
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}