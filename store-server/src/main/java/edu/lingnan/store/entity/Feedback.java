package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * (Feedback)实体类
 *
 * @author yinrui
 * @since 2021-04-15 00:08:05
 */
@Getter
@Setter
@ToString
public class Feedback implements Serializable {
    private static final long serialVersionUID = 154933891445460480L;
    
    private Integer id;
    
    private Integer userId;
    
    private Date createTime;
    
    private String text;
    
    private String contact;
    
    private String status;

}