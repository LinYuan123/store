package edu.lingnan.store.controller;

import edu.lingnan.store.common.ResultBean;
import edu.lingnan.store.service.MailService;
import edu.lingnan.store.utils.CodeUtil;
import edu.lingnan.store.utils.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("upload")
public class CommonController {

    private final String PICTURE_PATH = "D:\\DevelopmentTools\\nginx-1.18.0\\html\\images";
    private final String NGINX_SERVER_PATH = "http://localhost:88/upload/";

    @Autowired
    private UploadUtil uploadUtil;
    @Autowired
    private MailService mailService;

    /**
     * 图片上传
     * @param file
     * @return
     * @throws IOException
     */
    @PostMapping("pic")
    public ResultBean uploadPic(@RequestParam("file")MultipartFile file) throws IOException {
        String picName = uploadUtil.uploadPhoto(file,PICTURE_PATH);
        if(picName!=null){
            return new ResultBean("上传成功！",true,picName);
        }
        return new ResultBean("上传失败！",false,null);
    }

    @PostMapping("sendCode")
    public ResultBean sendCode(String mailTo){
        System.out.println("==========mailTo:"+mailTo);
        String contentF = "您好！感谢使用712便利店后台管理系统，本次您的收到验证码为：【";
        String contentE = "】，如非本人操作，请忽略此邮件。";
        String subject = "【712便利店后台管理系统】验证码";
        String code = CodeUtil.randomCode();
        try{
            mailService.sendCodeMail(mailTo,null,subject,contentF+code+contentE);
        } catch (Exception e){
            return new ResultBean("发送失败！",false,null);
        }
        return new ResultBean("发送成功！",true,code);
    }
}
