package edu.lingnan.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.entity.Feedback;

/**
 * (Feedback)表服务接口
 *
 * @author yinrui
 * @since 2021-04-15 00:08:05
 */
public interface FeedbackService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Feedback queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    IPage<Feedback> queryAllByLimit(int offset, int limit, Feedback feedback);

    /**
     * 新增数据
     *
     * @param feedback 实例对象
     * @return 实例对象
     */
    int insert(Feedback feedback);

    /**
     * 修改数据
     *
     * @param feedback 实例对象
     * @return 实例对象
     */
    int update(Feedback feedback);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    int changeFeedbackStatus(Integer[] ids, String status);

}