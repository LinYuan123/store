package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * (Menu)实体类
 *
 * @author makejava
 * @since 2021-03-28 16:39:46
 */
@Getter
@Setter
@ToString
public class Menu implements Serializable {
    private static final long serialVersionUID = 803518573323611739L;
    
    private Integer id;
    /**
    * 父级菜单ID
    */
    private Integer parentId;
    private String parentName;
    /**
    * 创建时间
    */
    private Date createTime;
    /**
    * 菜单标题
    */
    private String title;
    
    private Integer level;
    /**
    * 排序
    */
    private Integer sort;
    
    private String name;
    /**
    * 图标
    */
    private String icon;
    /**
    * 是否隐藏
    */
    private Integer hidden;

}