package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * (Goods)实体类
 *
 * @author yinrui
 * @since 2021-01-28 16:57:06
 */
@Getter
@Setter
@ToString
public class Goods extends QueryBean implements Serializable {
    private static final long serialVersionUID = 992472692349139152L;
    
    private Integer id;
    
    private String name;

    private Integer brandId;

    private String brandName;
    
    private Integer typeId;
    private Integer parentType;

    private String typeName;
    private String parentName;
    
    private Double price;
    
    private Integer stock;
    
    private Integer totalSales;
    
    private Integer monthlySales;
    
    private String status;
    /**
    * 收藏数量
    */
    private Integer focusNumber;
    /**
    * 图片地址
    */
    private String file;

    private Date createTime;

    private Integer isNew;

    private Integer isOnSale;

    private String detail;

}