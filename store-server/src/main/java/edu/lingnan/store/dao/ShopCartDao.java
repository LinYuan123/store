package edu.lingnan.store.dao;

import edu.lingnan.store.entity.ShopCart;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (ShopCart)表数据库访问层
 *
 * @author yinrui
 * @since 2021-04-10 14:04:29
 */
public interface ShopCartDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ShopCart queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ShopCart> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param shopCart 实例对象
     * @return 对象列表
     */
    List<ShopCart> queryAll(ShopCart shopCart);

    /**
     * 通过userId获取购物车列表
     * @param userId
     * @return
     */
    List<ShopCart> getCart(Integer userId);

    /**
     * 批量添加购物车
     * @param carts
     * @return
     */
    int setCart(@Param("carts")List<ShopCart> carts);

    /**
     * 通过userId删除购物车
     * @param userId
     * @return
     */
    int deleteCartByUserId(Integer userId);

    int deleteCartByUserIdAndGoodsId(@Param("userId")Integer userId,@Param("goodsId")Integer goodsId);

    /**
     * 新增数据
     *
     * @param shopCart 实例对象
     * @return 影响行数
     */
    int insert(ShopCart shopCart);

    /**
     * 修改数据
     *
     * @param shopCart 实例对象
     * @return 影响行数
     */
    int update(ShopCart shopCart);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}