package edu.lingnan.store.service;

import edu.lingnan.store.entity.ShopCart;

import java.util.List;

/**
 * (ShopCart)表服务接口
 *
 * @author yinrui
 * @since 2021-04-10 14:04:29
 */
public interface ShopCartService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ShopCart queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ShopCart> queryAllByLimit(int offset, int limit);

    /**
     * 通过userId获取购物车列表
     * @param userId
     * @return
     */
    List<ShopCart> getCart(Integer userId);

    /**
     * 批量添加购物车
     * @param carts
     * @return
     */
    int setCart(List<ShopCart> carts);

    /**
     * 通过userId删除购物车
     * @param userId
     * @return
     */
    int deleteCartByUserId(Integer userId);

    int deleteCartByUserIdAndGoodsId(Integer userId,Integer goodsId);

    /**
     * 新增数据
     *
     * @param shopCart 实例对象
     * @return 实例对象
     */
    ShopCart insert(ShopCart shopCart);

    /**
     * 修改数据
     *
     * @param shopCart 实例对象
     * @return 实例对象
     */
    ShopCart update(ShopCart shopCart);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}