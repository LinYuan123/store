//菜单相关请求
import axios from '../plugins/http.js'

/**
 * 获取菜单列表
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_menuList(params) {
  return axios({
    url: '/menu/list',
    method: 'get',
    params: params,
  })
}

/**
 * 返回所有父级菜单
 */
export function api_parentList() {
  return axios.get('menu/parentList')
}

/**
 * 获取菜单信息
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_menuInfo(params) {
  return axios({
    url: '/menu/info',
    method: 'get',
    params: params,
  })
}

/**
 * 保存菜单信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_saveMenu(params) {
  return axios({
    url: '/menu/save',
    method: 'post',
    data: params,
  })
}

/**
 * 删除菜单信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_deleteMenu(params) {
  return axios({
    url: '/menu/delete',
    method: 'post',
    data: params,
  })
}