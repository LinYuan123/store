package edu.lingnan.store.controller;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.common.ResultBean;
import edu.lingnan.store.entity.Feedback;
import edu.lingnan.store.service.FeedbackService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * (Feedback)表控制层
 *
 * @author yinrui
 * @since 2021-04-15 00:08:05
 */
@RestController
@RequestMapping("feedback")
public class FeedbackController {
    /**
     * 服务对象
     */
    @Resource
    private FeedbackService feedbackService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Feedback selectOne(Integer id) {
        return this.feedbackService.queryById(id);
    }

    /**
     * @param page
     * @param limit
     * @param bean
     * @return
     */
    @GetMapping("list")
    public ResultBean getFeedbackData(@RequestParam("page") Integer page,
                                   @RequestParam("limit") Integer limit,
                                   String bean){
        ResultBean resultBean = new ResultBean<>();
        Feedback feedback = JSONUtil.toBean(bean,Feedback.class);
        IPage<Feedback> iPage = feedbackService.queryAllByLimit(page,limit,feedback);
        Map<String, Object> map = new HashMap<>();
        map.put("total",iPage.getTotal());
        map.put("list",iPage.getRecords());
        resultBean.setData(map);
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

    @PostMapping("save")
    public ResultBean saveGoodsType(String bean){
        Feedback feedback = JSONUtil.toBean(bean,Feedback.class);
        ResultBean resultBean = new ResultBean();
        int count = 0;
        if(feedback.getId()!=null && feedback.getId()>0){
            count = feedbackService.update(feedback);
        }else{
            feedback.setStatus("NEW");
            feedback.setCreateTime(new Date());
            count = feedbackService.insert(feedback);
        }
        resultBean.setSuccess(count>0);
        if(count<=0){
            resultBean.setMsg("保存失败！");
            return resultBean;
        }
        resultBean.setMsg("保存成功！");
        return resultBean;
    }

    /**
     * 批量修改状态
     * @param map
     * @return
     */
    @PostMapping("updateStatus")
    public ResultBean updateStatus(@RequestBody HashMap<String, Object> map){
        ResultBean resultBean = new ResultBean<>();
        String status = (String) map.get("status");
        Integer[] ids = JSONUtil.toList(map.get("ids").toString(),Integer.class).toArray(new Integer[0]);
        if( ids == null || ids.length == 0){
            resultBean.setSuccess(false);
            resultBean.setMsg("请选择需要修改的数据！");
            return resultBean;
        }
        resultBean.setSuccess(feedbackService.changeFeedbackStatus(ids,status)>0);
        if(!resultBean.isSuccess()){
            resultBean.setMsg("操作失败！");
            return resultBean;
        }
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

}