// pages/address/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  wxAddress(){
    wx.chooseAddress({
      success: function (res) {
        var address = {
          "name": res.userName,
          "phone": res.telNumber,
          "province": res.provinceName,
          "city": res.cityName,
          "county": res.countyName,
          "detailInfo": res.detailInfo,
        };
        console.log(res);
        //  //获取到的地址存到data里的areaList中
        // that.setData({     
        //   areaList:that.data.areaList.push(address)
        // });
      },
      fail: () => {
        this.openConfirm()   // 如果获取地址权限失败，弹出确认弹窗，让用户选择是否要打开设置，手动去开权限
      }
    })
  }
})