package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * (ShopCart)实体类
 *
 * @author yinrui
 * @since 2021-04-10 14:04:29
 */
@Getter
@Setter
@ToString
public class ShopCart implements Serializable {
    private static final long serialVersionUID = -64999556099006654L;
    
    private Integer id;
    
    private Integer userId;
    
    private Integer amount;
    
    private Integer goodsId;

    private String goodsName;

    private Double price;

    private String file;

    private String brandName;
    
    private Date updateTime;

    private Boolean isCheck;

}