package edu.lingnan.store.controller;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.common.ResultBean;
import edu.lingnan.store.entity.GoodsType;
import edu.lingnan.store.entity.GoodsTypeWithChildren;
import edu.lingnan.store.service.GoodsTypeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (GoodsType)表控制层
 *
 * @author yinrui
 * @since 2021-01-28 16:57:06
 */
@RestController
@RequestMapping("goodsType")
public class GoodsTypeController {
    /**
     * 服务对象
     */
    @Resource
    private GoodsTypeService goodsTypeService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public GoodsType selectOne(Integer id) {
        return this.goodsTypeService.queryById(id);
    }

    @GetMapping("listWithChildren")
    public ResultBean listWithChildren(){
        ResultBean resultBean = new ResultBean();
        List<GoodsTypeWithChildren> list = this.goodsTypeService.listWithChildren();
        resultBean.setData(list);
        return resultBean;
    }

    @GetMapping("list")
    public ResultBean getProductTypeList(@RequestParam("page") Integer page,
                                         @RequestParam("limit") Integer limit,
                                         String bean){
        ResultBean resultBean = new ResultBean<>();
        GoodsType goods = JSONUtil.toBean(bean,GoodsType.class);
        IPage<GoodsType> iPage = goodsTypeService.queryAllByLimit(page,limit,goods);
        Map<String, Object> map = new HashMap<>();
        map.put("total",iPage.getTotal());
        map.put("list",iPage.getRecords());
        resultBean.setData(map);
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

    @GetMapping("parentList")
    public ResultBean parentTypeList(){
        ResultBean resultBean = new ResultBean();
        List<GoodsType> list = this.goodsTypeService.parentList();
        resultBean.setData(list);
        return resultBean;
    }

    @GetMapping("/info")
    public ResultBean<GoodsType> getGoodsTypeInfo(@RequestParam("id") Integer id) {
        ResultBean<GoodsType> resultBean = new ResultBean<>();
        if(id == null || id < 0){
            resultBean.setSuccess(false);
            return resultBean;
        }
        GoodsType goodsType = goodsTypeService.queryById(id);
        if(goodsType!=null){
            resultBean.setData(goodsType);
        }
        return resultBean;
    }

    @PostMapping("save")
    public ResultBean saveGoodsType(@RequestBody GoodsType bean){
        ResultBean resultBean = new ResultBean();
        int count = 0;
        if(bean.getId()!=null && bean.getId()>0){
            count = goodsTypeService.update(bean);
        }else{
            bean.setStatus("NORMAL");
            count = goodsTypeService.insert(bean);
        }
        resultBean.setSuccess(count>0);
        if(count<=0){
            resultBean.setMsg("保存失败！");
            return resultBean;
        }
        resultBean.setMsg("保存成功！");
        return resultBean;
    }

    @PostMapping("delete")
    public ResultBean delete(Integer id){
        ResultBean resultBean = new ResultBean<>();
        resultBean.setSuccess(goodsTypeService.deleteById(id));
        if(!resultBean.isSuccess()){
            resultBean.setMsg("删除失败！");
            return resultBean;
        }
        resultBean.setMsg("删除成功！");
        return resultBean;
    }

}