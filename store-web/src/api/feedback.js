//反馈相关请求
import axios from '../plugins/http.js'

/**
 * 获取订单列表
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_feedbackList(params) {
    return axios({
        url: '/feedback/list',
        method: 'get',
        params: params,
    })
}

/**
 * 修改订单状态
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_updateFeedbackStatus(params) {
    return axios({
        url: '/feedback/updateStatus',
        method: 'post',
        data: params,
    })
}