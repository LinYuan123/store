package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * (Role)实体类
 *
 * @author makejava
 * @since 2021-01-28 16:57:07
 */
@Getter
@Setter
@ToString
public class Role implements Serializable {
    private static final long serialVersionUID = -20348360090194673L;
    
    private Integer id;
    
    private String name;
    
    private String status;
    
    private Date createTime;

    private Integer level;

}