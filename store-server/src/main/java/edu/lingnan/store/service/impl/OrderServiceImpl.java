package edu.lingnan.store.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.lingnan.store.dao.OrderDao;
import edu.lingnan.store.dao.OrderDetailDao;
import edu.lingnan.store.entity.Goods;
import edu.lingnan.store.entity.Order;
import edu.lingnan.store.entity.OrderDetail;
import edu.lingnan.store.service.OrderService;
import edu.lingnan.store.utils.OrderCodeUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Order)表服务实现类
 *
 * @author makejava
 * @since 2021-01-28 16:57:06
 */
@Service("orderService")
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderDao orderDao;
    @Resource
    private OrderDetailDao orderDetailDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Order queryById(String id) {
        return this.orderDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public IPage<Order> queryAllByLimit(int offset, int limit, Order bean) {
        Page<Order> page = new Page<>(offset, limit);
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        wrapper.like("address",bean.getAddress())
                .eq("pickup_way",bean.getPickupWay())
                .gt("create_time",bean.getBeginTime())
                .lt("create_time",bean.getEndTime());
        page.setRecords(orderDao.queryAll(page,bean));
        return page;
    }

    @Override
    public List<Order> queryOrderByUserIdAndStatus(Integer userId, String status){
        return this.orderDao.queryOrderByUserIdAndStatus(userId, status);
    }

    @Override
    public List<Order> getOrdersByStatusAndPickupWay(String pickupWay, String status){
        return this.orderDao.getOrdersByStatusAndPickupWay(pickupWay, status);
    }

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(Order order) {
        return this.orderDao.insert(order);
    }

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    @Override
    public int update(Order order) {
        return this.orderDao.update(order);
    }

    @Override
    public String save(Order order, List<OrderDetail> orderDetails){
        int count= 0;
        boolean flag = true;
        String orderId = OrderCodeUtil.getOrderCode(order.getUserId());
        if(order.getId()==null){
            order.setId(orderId);
            count = this.orderDao.insert(order);
        }else{
            count = this.orderDao.update(order);
        }
        if(count>0){
            flag = this.deleteByOrderId(order.getId());
            for(OrderDetail detail:orderDetails){
                detail.setOrderId(order.getId());
                flag = this.orderDetailDao.insert(detail) > 0;
            }
        }
        if(!flag){
            return null;
        }
        return order.getId();
    }

    @Override
    public boolean changeOrderStatus(String[] ids, String status) {
        return this.orderDao.changeOrderStatus(ids,status) > 0;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.orderDao.deleteById(id) > 0;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public OrderDetail queryById(Integer id) {
        return this.orderDetailDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<OrderDetail> queryAllByLimit(int offset, int limit) {
        return this.orderDetailDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param orderDetail 实例对象
     * @return 实例对象
     */
    @Override
    public OrderDetail insert(OrderDetail orderDetail) {
        this.orderDetailDao.insert(orderDetail);
        return orderDetail;
    }

    /**
     * 修改数据
     *
     * @param orderDetail 实例对象
     * @return 实例对象
     */
    @Override
    public OrderDetail update(OrderDetail orderDetail) {
        this.orderDetailDao.update(orderDetail);
        return this.queryById(orderDetail.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteByOrderId(String id) {
        return this.orderDetailDao.deleteByOrderId(id) > 0;
    }
}