package edu.lingnan.store.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.entity.Goods;
import edu.lingnan.store.entity.OrderDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Goods)表数据库访问层
 *
 * @author yinrui
 * @since 2021-01-28 16:57:06
 */
public interface GoodsDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Goods queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Goods> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit, @Param("goods") Goods goods);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param goods 实例对象
     * @return 对象列表
     */
    List<Goods> queryAll(IPage<Goods> page, @Param("goods") Goods goods);

    List<Goods> getAllProduct();

    List<Goods> getAllNew();

    List<Goods> getStock(@Param("details")List<OrderDetail> details);

    //低库存
    List<Goods> getLowStocks();

    //销量前十
    List<Goods> getSalesRank();

    int updateGoodsSale(@Param("goodsId")Integer goodsId,@Param("num")Integer num);

    int resetMonthlySales();

    /**
     * 新增数据
     *
     * @param goods 实例对象
     * @return 影响行数
     */
    int insert(Goods goods);

    /**
     * 修改数据
     *
     * @param goods 实例对象
     * @return 影响行数
     */
    int update(Goods goods);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    /**
     * 批量修改状态
     * @param ids
     * @param status
     * @return
     */
    int changeGoodsStatus(@Param("ids") Integer[] ids, @Param("type") String type, @Param("status") Integer status);

}