package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * (Brand)实体类
 *
 * @author yinrui
 * @since 2021-04-03 14:15:52
 */
@Getter
@Setter
@ToString
public class Brand extends QueryBean implements Serializable {
    private static final long serialVersionUID = 758962551682793389L;
    
    private Integer id;
    /**
    * 品牌名称
    */
    private String name;
    
    private Date createTime;
    
    private String status;

}