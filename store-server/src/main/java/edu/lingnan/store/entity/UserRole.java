package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * (UserRole)实体类
 *
 * @author yinrui
 * @since 2021-04-13 23:54:23
 */
@Getter
@Setter
@ToString
public class UserRole implements Serializable {
    private static final long serialVersionUID = -36761288160820803L;
    
    private Integer id;
    
    private Integer userId;

    private String loginName;
    
    private Integer roleId;

    private String roleName;

}