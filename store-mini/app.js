import { request } from "request/index.js";
App({

  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function () {
    //做token过期验证
    this.refreshToken()
  },

  async refreshToken(){
    const token = wx.getStorageSync('token')
    if(token){
      const res = await request({ url: "user/refreshToken", data: {token:token}, method: 'POST' });
      if (res.success) {
        //token已刷新
        let newtoken = res.data.tokenHead + res.data.token
        wx.setStorageSync('token', newtoken)
      }else{
        //清除本地缓存
        wx.clearStorage()
        wx.clearStorageSync()
      }
    }
  },
  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (options) {
    
  },

  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function () {
    
  },

  /**
   * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
   */
  onError: function (msg) {
    
  },

  //options(path,query,isEntryPage)
  onPageNotFound: function(options){

  }

})
