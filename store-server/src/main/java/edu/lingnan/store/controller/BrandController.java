package edu.lingnan.store.controller;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.common.ResultBean;
import edu.lingnan.store.entity.Brand;
import edu.lingnan.store.service.BrandService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Brand)表控制层
 *
 * @author yinrui
 * @since 2021-04-03 14:15:52
 */
@RestController
@RequestMapping("brand")
public class BrandController {
    /**
     * 服务对象
     */
    @Resource
    private BrandService brandService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Brand selectOne(Integer id) {
        return this.brandService.queryById(id);
    }

    /**
     * 根据条件查询商品数据
     * @param page
     * @param limit
     * @param bean
     * @return
     */
    @GetMapping("list")
    public ResultBean getBrandData(@RequestParam("page") Integer page,
                                   @RequestParam("limit") Integer limit,
                                   String bean){
        ResultBean resultBean = new ResultBean<>();
        Brand brand = JSONUtil.toBean(bean,Brand.class);
        IPage<Brand> iPage = brandService.queryAllByLimit(page,limit,brand);
        Map<String, Object> map = new HashMap<>();
        map.put("total",iPage.getTotal());
        map.put("list",iPage.getRecords());
        resultBean.setData(map);
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

    @GetMapping("brandList")
    public ResultBean getBrandList(){
        ResultBean resultBean = new ResultBean<>();
        List<Brand> list = brandService.queryAllBrand();
        resultBean.setData(list);
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

    @GetMapping("/info")
    public ResultBean<Brand> getBrandInfo(@RequestParam("id") Integer id) {
        ResultBean<Brand> resultBean = new ResultBean<>();
        if(id == null || id < 0){
            resultBean.setSuccess(false);
            return resultBean;
        }
        Brand goodsType = brandService.queryById(id);
        if(goodsType!=null){
            resultBean.setData(goodsType);
        }
        return resultBean;
    }

    @PostMapping("save")
    public ResultBean saveGoodsType(@RequestBody Brand bean){
        ResultBean resultBean = new ResultBean();
        int count = 0;
        if(bean.getId()!=null && bean.getId()>0){
            count = brandService.update(bean);
        }else{
            bean.setStatus("NORMAL");
            count = brandService.insert(bean);
        }
        resultBean.setSuccess(count>0);
        if(count<=0){
            resultBean.setMsg("保存失败！");
            return resultBean;
        }
        resultBean.setMsg("保存成功！");
        return resultBean;
    }

    @PostMapping("delete")
    public ResultBean delete(Integer id){
        ResultBean resultBean = new ResultBean<>();
        resultBean.setSuccess(brandService.deleteById(id));
        if(!resultBean.isSuccess()){
            resultBean.setMsg("删除失败！");
            return resultBean;
        }
        resultBean.setMsg("删除成功！");
        return resultBean;
    }

}