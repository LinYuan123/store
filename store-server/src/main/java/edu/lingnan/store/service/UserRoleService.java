package edu.lingnan.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.entity.UserRole;

/**
 * (UserRole)表服务接口
 *
 * @author yinrui
 * @since 2021-04-13 23:54:23
 */
public interface UserRoleService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserRole queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    IPage<UserRole> queryAllByLimit(int offset, int limit,UserRole bean);

    /**
     * 新增数据
     *
     * @param userRole 实例对象
     * @return 实例对象
     */
    int insert(UserRole userRole);

    /**
     * 修改数据
     *
     * @param userRole 实例对象
     * @return 实例对象
     */
    int update(UserRole userRole);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}