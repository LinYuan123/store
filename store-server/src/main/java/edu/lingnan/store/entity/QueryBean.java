package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QueryBean {

    /**
     * 查询时间
     */
    private String beginTime;
    private String endTime;
    private String startDate;
    private String endDate;
    private Integer startNum;
    private Integer endNum;

}
