package edu.lingnan.store.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.lingnan.store.common.ResultBean;
import edu.lingnan.store.dao.UserDao;
import edu.lingnan.store.entity.User;
import edu.lingnan.store.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (User)表服务实现类
 *
 * @author makejava
 * @since 2021-01-28 16:57:07
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    @Override
    public ResultBean<User> login(User user){
        User result = userDao.login(user);
        ResultBean<User> resultBean = new ResultBean<>();
        if(result==null){
            resultBean.setCode(404);
            resultBean.setSuccess(false);
            resultBean.setMsg("账号或密码错误，请重试！");
            return resultBean;
        }
        resultBean.setMsg("登陆成功");
        resultBean.setData(result);
        return resultBean;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public User queryById(Integer id) {
        return this.userDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public IPage<User> queryAllByLimit(int offset, int limit,User bean) {
        Page<User> page = new Page<>(offset, limit);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like("login_name",bean.getLoginName())
                .like("name",bean.getName())
                .eq("contact",bean.getContact())
                .gt("create_time",bean.getBeginTime())
                .lt("create_time",bean.getEndTime());
        page.setRecords(userDao.queryAll(page,bean));
        return page;
    }

    @Override
    public List<User> getNoRoleUsers(){
        return this.userDao.getNoRoleUsers();
    }

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(User user) {
        return this.userDao.insert(user);
    }

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public int update(User user) {
        return this.userDao.update(user);
    }

    /**
     * 批量修改状态
     *
     * @param ids 实例对象
     * @return 实例对象
     */
    @Override
    public int changeUserStatus(Integer[] ids,String status){
        return this.userDao.changeUserStatus(ids,status);
    }

    @Override
    public int updateUserIntegral(Integer integral, Integer userId){
        return this.userDao.updateUserIntegral(integral, userId);
    }

    @Override
    public int getUserIntegral(Integer userId){
        return this.userDao.getUserIntegral(userId);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.userDao.deleteById(id) > 0;
    }

    @Override
    public User loadUserByLoginName(String loginName) {
        return this.userDao.loadUserByLoginName(loginName);
    }

    @Override
    public List<User> checkUserExist(User user){
        return this.userDao.checkUserExist(user);
    }

    @Override
    public List<User> checkUserRight(User user){
        return this.userDao.checkUserRight(user);
    }
}