package edu.lingnan.store.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.lingnan.store.dao.BrandDao;
import edu.lingnan.store.entity.Brand;
import edu.lingnan.store.service.BrandService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Brand)表服务实现类
 *
 * @author yinrui
 * @since 2021-04-03 14:15:52
 */
@Service("brandService")
public class BrandServiceImpl implements BrandService {
    @Resource
    private BrandDao brandDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Brand queryById(Integer id) {
        return this.brandDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public IPage<Brand> queryAllByLimit(int offset, int limit,Brand bean) {
        Page<Brand> page = new Page<>(offset, limit);
        QueryWrapper<Brand> wrapper = new QueryWrapper<>();
        wrapper.like("name",bean.getName())
                .gt("create_time",bean.getBeginTime())
                .lt("create_time",bean.getEndTime());
        page.setRecords(brandDao.queryAll(page,bean));
        page.setTotal(page.getRecords().size());
        return page;
    }

    @Override
    public List<Brand> queryAllBrand(){
        return this.brandDao.queryAllBrand();
    }

    /**
     * 新增数据
     *
     * @param brand 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(Brand brand) {
        return this.brandDao.insert(brand);
    }

    /**
     * 修改数据
     *
     * @param brand 实例对象
     * @return 实例对象
     */
    @Override
    public int update(Brand brand) {
        return this.brandDao.update(brand);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.brandDao.deleteById(id) > 0;
    }
}