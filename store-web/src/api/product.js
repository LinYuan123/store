//商品相关请求
import axios from '../plugins/http.js'

/**
 * 获取商品列表
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_productList(params) {
  return axios({
    url: '/product/list',
    method: 'get',
    params: params,
  })
}

export function api_lowStocksList() {
  return axios({
    url: '/product/lowStocks',
    method: 'get',
  })
}

export function api_salesRankList() {
  return axios({
    url: '/product/salesRank',
    method: 'get',
  })
}

/**
 * 修改商品上架，新品状态
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_updateProductStatus(params) {
  return axios({
    url: '/product/updateStatus',
    method: 'post',
    data: params,
  })
}

/**
 * 获取商品信息
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_productInfo(params) {
  return axios({
    url: '/product/info',
    method: 'get',
    params: params,
  })
}

/**
 * 保存商品信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_saveProduct(params) {
  return axios({
    url: '/product/save',
    method: 'post',
    data: params,
  })
}

export function api_deleteProduct(params) {
  return axios({
    url: '/product/delete',
    method: 'post',
    data: params,
  })
}
