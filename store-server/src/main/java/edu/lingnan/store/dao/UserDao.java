package edu.lingnan.store.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (User)表数据库访问层
 *
 * @author makejava
 * @since 2021-01-28 16:57:07
 */
public interface UserDao {

    User login(@Param("user") User user);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<User> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit, @Param("user") User user);


    /**
     * 通过实体作为筛选条件查询
     *
     * @Param page 页码
     * @param user 实例对象
     * @return 对象列表
     */
    List<User> queryAll(IPage<User> page, @Param("user") User user);

    List<User> getNoRoleUsers();

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int insert(User user);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int update(User user);

    /**
     * 批量修改状态
     * @param ids
     * @param status
     * @return
     */
    int changeUserStatus(@Param("ids") Integer[] ids, @Param("status") String status);

    /**
     * 更新用户积分
     * @param integral
     * @param userId
     * @return
     */
    int updateUserIntegral(@Param("integral")Integer integral,@Param("userId")Integer userId);

    int getUserIntegral(Integer userId);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    /**
     * 根据Usernamem获取用户名密码
     * @param loginName
     * @return
     */
    User loadUserByLoginName(String loginName);

    List<User> checkUserExist(User user);

    List<User> checkUserRight(User user);
}