import { request } from "../../request/index.js";
Page({
  data:{
    isLogin:true,
  },
  regClick(){
    this.setData({
      isLogin:false
    })
  },
  async formSubmit(e){
    let userInfo = null;
    let user= null;
    let that= this;
    if(this.data.isLogin){
      //登录方法
      user= {
        loginAccount:e.detail.value.loginAccount,
        password:e.detail.value.password
      }
      const res = await request({ url: "user/login", data: user, method: 'POST' });
      if(res.success){
        wx.showToast({
          title: '登录成功！',
          icon: 'none',
          mask: true
        });
        user = res.data.user;
        wx.setStorageSync("token", res.data.tokenHead+res.data.token);
        wx.getUserProfile({
          desc:'获取用户信息',
          success:res=>{
            userInfo=res.userInfo
            let userSave = {
              id:user.id,
              address:user.address,
              contact:user.contact,
              email:user.email,
              integral:user.integral,
              loginName:userInfo.nickName,
              avatarUrl:userInfo.avatarUrl
            }
            wx.setStorageSync("userinfo", userSave);
            console.log(userSave)
            wx.navigateBack({
              delta: 1
            });
          }
        })
      }else{
        wx.showToast({
          title: '账号或密码错误！',
          icon: 'none',
          mask: true
        });
      }
    }else{
      //注册方法
      let em = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
      let ph = /^[1][34578]\d{9}$/;
      if(!e.detail.value.loginName){
        wx.showToast({
          title: '登录账号不能为空！',
          icon: 'none',
          mask: true
        });
        return
      }
      if(!e.detail.value.contact){
        wx.showToast({
          title: '手机不能为空！',
          icon: 'none',
          mask: true
        });
        return
      }else if(!e.detail.value.email){
        wx.showToast({
          title: '邮箱号不能为空！',
          icon: 'none',
          mask: true
        });
        return
      }else if(!e.detail.value.password){
        wx.showToast({
          title: '密码不能为空！',
          icon: 'none',
          mask: true
        });
        return
      }
      if(!em.test(e.detail.value.email)){
        wx.showToast({
          title: '请输入正确的邮箱！',
          icon: 'none',
          mask: true
        });
        return
      }else if(!ph.test(e.detail.value.contact)){
        wx.showToast({
          title: '请输入正确的手机号！',
          icon: 'none',
          mask: true
        });
        return
      }
      user= {
        loginName:e.detail.value.loginName,
        contact:e.detail.value.contact,
        email:e.detail.value.email,
        password:e.detail.value.password
      }
      const result = await request({ url: "user/register", data: user, method: 'POST' });
      if(result.success){
        wx.showToast({
          title: '注册成功！',
          icon: 'none',
          mask: true
        });
        user = result.data;
        that.setData({
          isLogin:true
        })
      }else{
        wx.showToast({
          title: result.msg,
          icon: 'none',
          mask: true
        });
      }
    }
    
  },
  back(){
    this.setData({
      isLogin:true
    })
  }
})