//用户管理相关请求
import axios from '../plugins/http.js'

/**
 * 返回所有品牌列表
 */
export function api_roleList() {
    return axios.get('role/list')
}
/**
 * 返回所有用户列表
 */
export function api_userList(params) {
    return axios({
        url: '/user/list',
        method: 'get',
        params: params,
    })
}

/**
 * 获取用户信息
 * @returns {Promise<AxiosResponse<T>>}
 */
export function api_userInfo(params) {
    return axios({
        url: '/user/userInfo',
        method: 'get',
        params: params,
    })
}

/**
 * 保存用户信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_saveUser(params) {
    return axios({
        url: '/user/save',
        method: 'post',
        data: params,
    })
}

/**
 * 删除用户信息
 * @param params 参数
 * @returns {AxiosPromise}
 */
export function api_deleteUser(params) {
    return axios({
        url: '/user/delete',
        method: 'post',
        data: params,
    })
}
