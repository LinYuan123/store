package edu.lingnan.store.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.lingnan.store.dao.UserRoleDao;
import edu.lingnan.store.entity.UserRole;
import edu.lingnan.store.service.UserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (UserRole)表服务实现类
 *
 * @author yinrui
 * @since 2021-04-13 23:54:23
 */
@Service("userRoleService")
public class UserRoleServiceImpl implements UserRoleService {
    @Resource
    private UserRoleDao userRoleDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public UserRole queryById(Integer id) {
        return this.userRoleDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public IPage<UserRole> queryAllByLimit(int offset, int limit,UserRole bean) {
        Page<UserRole> page = new Page<>(offset, limit);
//        QueryWrapper<UserRole> wrapper = new QueryWrapper<>();
        page.setRecords(userRoleDao.queryAll(page,bean));
        page.setTotal(page.getRecords().size());
        return page;
    }

    /**
     * 新增数据
     *
     * @param userRole 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(UserRole userRole) {
        return this.userRoleDao.insert(userRole);
    }

    /**
     * 修改数据
     *
     * @param userRole 实例对象
     * @return 实例对象
     */
    @Override
    public int update(UserRole userRole) {
        return this.userRoleDao.update(userRole);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.userRoleDao.deleteById(id) > 0;
    }
}