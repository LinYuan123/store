package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * (OrderDetail)实体类
 *
 * @author makejava
 * @since 2021-01-28 16:57:07
 */
@Getter
@Setter
@ToString
public class OrderDetail implements Serializable {
    private static final long serialVersionUID = -48455932746586452L;
    
    private Integer id;
    
    private String orderId;
    
    private Integer goodsId;

    private String goodsName;

    private Double goodsPrice;

    private String file;

    private String brandName;
    
    private Integer amount;

}