package edu.lingnan.store.controller;

import edu.lingnan.store.common.ResultBean;
import edu.lingnan.store.entity.Role;
import edu.lingnan.store.service.RoleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Role)表控制层
 *
 * @author makejava
 * @since 2021-01-28 16:57:07
 */
@RestController
@RequestMapping("role")
public class RoleController {
    /**
     * 服务对象
     */
    @Resource
    private RoleService roleService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Role selectOne(Integer id) {
        return this.roleService.queryById(id);
    }

    @GetMapping("roleList")
    public ResultBean getRoleList(){
        ResultBean resultBean = new ResultBean<>();
        List<Role> list = roleService.queryAllRole();
        resultBean.setData(list);
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

}