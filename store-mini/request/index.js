//异步请求次数
let ajaxTimes = 0;
export const request = (params)=>{

  // console.log(params);

  //非login或goods则带上token进行请求
  let header = {...params.header };
  if(!params.url.includes("login","product","register","index/index","goodsType")
    ){
      if(params.method==="POST" && !params.url.includes("refreshToken")){
        header["Content-Type"] = "application/x-www-form-urlencoded";
      }
      header["Authorization"] = wx.getStorageSync("token");
  }

  ajaxTimes++;
  //显示加载中效果
  wx.showLoading({
    title: '加载中',
    mask:true
  })

  // const baseUrl="http://192.168.3.3:8081/";
  const baseUrl="http://localhost:8081/";
  return new Promise((resolve,reject)=>{
    wx.request({
      ...params,
      header:header,
      url:baseUrl+params.url,
      method:params.method,
      success:(result)=>{
        resolve(result.data);
      },
      fail:(err)=>{
        reject(err);
      },
      complete:()=>{
        ajaxTimes--;
        if(ajaxTimes<=0){
          wx.hideLoading();
        }
      }
    })
  })


}