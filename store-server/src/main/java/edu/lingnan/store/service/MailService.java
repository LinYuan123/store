package edu.lingnan.store.service;

public interface MailService {

    void sendCodeMail(String mailTo,String cc,String subject,String content);

}
