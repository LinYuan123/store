package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author yinrui
 */
public class GoodsTypeWithChildren extends GoodsType {

    @Getter
    @Setter
    private List<GoodsType> children;

}
