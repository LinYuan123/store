package edu.lingnan.store.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Order)表数据库访问层
 *
 * @author makejava
 * @since 2021-01-28 16:57:06
 */
public interface OrderDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Order queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Order> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit, @Param("order") Order order);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param order 实例对象
     * @return 对象列表
     */
    List<Order> queryAll(IPage<Order> page, @Param("order") Order order);

    List<Order> queryOrderByUserIdAndStatus(@Param("userId")Integer userId, @Param("status")String status);

    List<Order> getOrdersByStatusAndPickupWay(@Param("pickupWay")String pickupWay,@Param("status")String status);

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    int insert(@Param("order") Order order);

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    int update(@Param("order") Order order);

    /**
     * 批量修改状态
     *
     * @param ids
     * @param status
     */
    int changeOrderStatus(@Param("ids") String[] ids, @Param("status") String status);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}