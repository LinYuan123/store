package edu.lingnan.store.controller;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import edu.lingnan.store.common.ResultBean;
import edu.lingnan.store.entity.Goods;
import edu.lingnan.store.service.GoodsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * (Goods)表控制层
 *
 * @author yinrui
 * @since 2021-01-28 16:57:06
 */
@RestController
@RequestMapping("product")
public class GoodsController {
    /**
     * 服务对象
     */
    @Resource
    private GoodsService goodsService;

    /**
     * 根据条件查询商品数据
     * @param page
     * @param limit
     * @param bean
     * @return
     */
    @GetMapping("list")
    public ResultBean getProductsData(@RequestParam("page") Integer page,
                                   @RequestParam("limit") Integer limit,
                                   String bean){
        ResultBean resultBean = new ResultBean<>();
        Goods goods = JSONUtil.toBean(bean,Goods.class);
        IPage<Goods> iPage = goodsService.queryAllByLimit(page,limit,goods);
        Map<String, Object> map = new HashMap<>();
        map.put("total",iPage.getTotal());
        map.put("list",iPage.getRecords());
        resultBean.setData(map);
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

    @GetMapping("allProduct")
    public ResultBean getGoodsData(){
        ResultBean resultBean = new ResultBean<>();
        Map<String, Object> map = new HashMap<>();
        map.put("allProduct",goodsService.getAllProduct());
        map.put("allNew",goodsService.getAllNew());
        resultBean.setData(map);
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

    /**
     * 库存告急
     * @return
     */
    @GetMapping("lowStocks")
    public ResultBean getLowStocks(){
        ResultBean resultBean = new ResultBean<>();
        resultBean.setData(goodsService.getLowStocks());
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

    /**
     * 销量前十
     * @return
     */
    @GetMapping("salesRank")
    public ResultBean getSalesRank(){
        ResultBean resultBean = new ResultBean<>();
        resultBean.setData(goodsService.getSalesRank());
        resultBean.setMsg("操作成功！");
        return resultBean;
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/info")
    public ResultBean<Goods> getGoodsInfo(@RequestParam("id") Integer id) {
        ResultBean<Goods> resultBean = new ResultBean<>();
        if(id == null || id < 0){
            resultBean.setSuccess(false);
            return resultBean;
        }
        Goods goods = goodsService.queryById(id);
        if(goods!=null){
            resultBean.setData(goods);
        }
        return resultBean;
    }

    /**
     * 保存商品信息
     * @param bean
     * @return
     */
    @PostMapping("save")
    public ResultBean saveGoods(@RequestBody Goods bean){
        ResultBean resultBean = new ResultBean();
        int count = 0;
        if(bean.getId()!=null && bean.getId()>0){
            count = goodsService.update(bean);
        }else{
            bean.setCreateTime(new Date());
            bean.setStatus("NORMAL");
            bean.setTotalSales(0);
            bean.setMonthlySales(0);
            bean.setFocusNumber(0);
            count = goodsService.insert(bean);
        }
        resultBean.setSuccess(count>0);
        if(count<=0){
            resultBean.setMsg("保存失败！");
            return resultBean;
        }
        resultBean.setMsg("保存成功！");
        return resultBean;
    }

    @PostMapping("delete")
    public ResultBean delete(Integer id){
        ResultBean resultBean = new ResultBean<>();
        resultBean.setSuccess(goodsService.deleteById(id));
        if(!resultBean.isSuccess()){
            resultBean.setMsg("删除失败！");
            return resultBean;
        }
        resultBean.setMsg("删除成功！");
        return resultBean;
    }

    /**
     * 批量修改状态（上新，上架
     * @param map
     * @return
     */
    @PostMapping("updateStatus")
    public ResultBean updateStatus(@RequestBody HashMap<String, Object> map){
        ResultBean resultBean = new ResultBean<>();
        //{ids=[1], type=new, status=0}
        String type = map.get("type").toString();
        Integer status = (Integer) map.get("status");
        Integer[] ids = JSONUtil.toList(map.get("ids").toString(),Integer.class).toArray(new Integer[0]);
        if( ids == null || ids.length == 0){
            resultBean.setSuccess(false);
            resultBean.setMsg("请选择需要修改的数据！");
            return resultBean;
        }
        resultBean.setSuccess(goodsService.changeGoodsStatus(ids,type,status)>0);
        if(!resultBean.isSuccess()){
            resultBean.setMsg("修改失败！");
            return resultBean;
        }
        resultBean.setMsg("修改成功！");
        return resultBean;
    }

}