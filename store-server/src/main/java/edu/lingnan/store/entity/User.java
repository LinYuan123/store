package edu.lingnan.store.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * (User)实体类
 *
 * @author makejava
 * @since 2021-01-28 16:57:07
 */
@Getter
@Setter
@ToString
public class User extends QueryBean implements Serializable {
    private static final long serialVersionUID = 331545584704703398L;

    @Id
    private Integer id;
    
    private String loginName;
    
    private String name;
    
    private String password;
    
    private Date createTime;
    
    private Date lastLoginTime;
    
    private String sex;
    /**
    * 积分
    */
    private Integer integral;
    /**
     * 联系方式
     */
    private String contact;
    /**
     * 邮件地址
     */
    private String email;
    
    private String address;
    /**
    * 头像图片地址
    */
    private String headPortrait;
    /**
    * 账号状态 正常-NORMAL 冻结-FREEZE 删除-DELETE
    */
    private String status;

    /**
     * 三合一登录方式登录账号
     */
    private String loginAccount;

    /**
     * 角色id
     */
    private Integer roleId;

    private String roleName;

}