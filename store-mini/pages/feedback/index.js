import { request } from "../../request/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    textVal:'',
    textareaVal:''
  },
  onShow() {
    const userInfo = wx.getStorageSync('userinfo')
    if(userInfo===null || userInfo===""){
      wx.navigateTo({
        url: '/pages/login/index'
      })
    }
  },
  handleTextInput(e){
    this.setData({
      textVal:e.detail.value
    })
  },
  handleTextareaInput(e){
    this.setData({
      textareaVal:e.detail.value
    })
  },
  async handleSubmit(){
    const { textVal, textareaVal } = this.data;
    //意见项非空判断
    if (!textareaVal.trim()) {
      wx.showToast({
          title: '请输入您的问题或意见',
          icon: 'none',
          mask: true
      });
      return;
    }
    //联系方式格式验证
    var phoneReg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
    if (textVal.trim()) {
      if(!phoneReg.test(textVal)){
        wx.showToast({
          title: '请输入正确的手机号',
          icon: 'none',
          mask: true
        });
      return;
      }
    }
    //提交到后台
    console.log(textVal+"==="+textareaVal)
    const userInfo = wx.getStorageSync('userinfo')
    let feedback = {
      userId:userInfo.id,
      text:textareaVal,
      contact:textVal
    }
    const res = await request({ url: "feedback/save", data: {bean:JSON.stringify(feedback)}, method: 'POST' });
    wx.showToast({
      title: res.msg,
      icon: 'none',
      mask: true
    });
    //回到个人信息页
    if(res.success){
      wx.switchTab({
        url: '/pages/user/index'
      })
    }
  }
})